<?php

use App\User;
use App\Address;
use App\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| One to One Relationship
|--------------------------------------------------------------------------
|
*/
// Route::get('/insert', function () {
//     $user = User::findOrFail(1);

//     $address = new Address(['name' => '1234 USA Street Kingsman Ave.']);

//     $user->address()->save($address);
// });

// Route::get('/update', function () {
//     $address = Address::whereUserId(1)->first();

//     $address->name = "1234 Updated USA Street Kingsman Ave.";

//     $address->save();
// });

// Route::get('/read', function () {
//     $user = User::findOrFail(1);

//     return $user->address->name;
// });

// Route::get('/delete', function () {
//     $address = Address::findOrFail(1);

//     $address->delete();
// });

/*
|--------------------------------------------------------------------------
| One to Many Relationship
|--------------------------------------------------------------------------
|
*/
Route::get('/create', function () {
    $user = User::findOrFail(1);

    $post = new Post(['title' => 'Laravel Title 01', 'body' => 'This is the laravel title 01']);

    $user->posts()->save($post);
});

Route::get('/read', function () {
    $user = User::findOrFail(1);

    // return $user->posts;
    foreach ($user->posts as $post) {
        echo $post->title . "<br>";
    }
});

Route::get('/update', function () {
    $user = User::find(1);

    $user->posts()->whereId(1)->update(['title' => 'Updated Laravel Title', 'body' => 'Updated Content laraveel title']);
});

Route::get('/delete', function () {
    $user = User::find(1);

    $user->posts()->whereId(1)->delete();
});