<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersRequest;
use App\Http\Requests\UserEditRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Role;

class AdminUsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = User::orderBy('id', 'DESC')->paginate(10);

        return view('admin.users.index', compact('users'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $roles = Role::all();

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {

        if( $request->hasFile('user_profile') ){
            // get the file name with extension
            $fileNameExtension = $request->file('user_profile')->getClientOriginalName();

            // get just the file name
            $fileName = pathinfo($fileNameExtension, PATHINFO_FILENAME);

            // get the extension
            $extension = $request->file('user_profile')->getClientOriginalExtension();

            // file name to store
            $fileNameStore= $fileName.'_'.time().'.'.$extension;

            // upload the image
            $path = $request->file('user_profile')->storeAs('public/user_profile', $fileNameStore);
        }else{
            $fileNameStore = 'noimage.png';
        }

        if( trim($request->password) == '' ){
            $user->password = $request->except('password');
        }else{

            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt( $request->password );
            $user->role_id = $request->input('role_id');
            $user->is_active = $request->input('is_active');
            $user->user_profile = $fileNameStore;
            $user->save();

        }

        return redirect('admin/users')->with('status', 'User created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.users.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $roles = Role::all();

        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditRequest $request, $id)
    {
        if( $request->hasFile('user_profile') ){
            // get the file name with extension
            $fileNameExtension = $request->file('user_profile')->getClientOriginalName();

            // get just the file name
            $fileName = pathinfo($fileNameExtension, PATHINFO_FILENAME);

            // get the extension
            $extension = $request->file('user_profile')->getClientOriginalExtension();

            // file name to store
            $fileNameStore= $fileName.'_'.time().'.'.$extension;

            // upload the image
            $path = $request->file('user_profile')->storeAs('public/user_profile', $fileNameStore);
        }else{
            $fileNameStore = 'noimage.png';
        }

        if( trim($request->password) == '' ){
            $user->password = $request->except('password');
        }else{

            $user = User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt( $request->password );
            $user->role_id = $request->input('role_id');
            $user->is_active = $request->input('is_active');

            if( $request->hasFile('user_profile') ){
                $user->user_profile = $fileNameStore;
            }
            
            $user->save();

        }

        return redirect('admin/users')->with('status', 'User updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $user =  User::find($id);

        if( $user->user_profile != 'noimage.jpg' ){
            Storage::delete('public/user_profile/' . $user->user_profile);
        }

        $user->delete();

        return redirect('admin/users')->with('status', 'User deleted successfully!');
    }
}
