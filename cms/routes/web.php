<?php

Use App\User;
Use App\Post;
Use App\Country;
Use App\Photo;
Use App\Tag;
Use App\Taggable;
Use App\Video;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| CRUD Applications
|--------------------------------------------------------------------------
|
*/

// Posts Route
Route::group(['middleware' => ['web']], function () {
    Route::resource('posts', 'PostsController');
});

// Pages Route
Route::get('contact', 'PagesController@contact');

/*
|--------------------------------------------------------------------------
| Eloquent Relationship
|--------------------------------------------------------------------------
|
*/

// hasOne relationship or One to One Relationship
// Route::get('user/{id}/post', function ($id) {
//     return User::find($id)->post;
// });

// Route::get('post/{id}/user', function ($id) {
//     return Post::find($id)->user->name;
// });

// One to Many Relationship
// Route::get('/posts', function () {
//     $user = User::find(1);
    
//     foreach ($user->posts as $post) {
//         echo $post->title . "<br>";
//     }

// });

// Many to Many Relationship
// Route::get('/user/{id}/role', function ($id) {
//     $user = User::find($id)->roles()->orderBy('id', 'desc')->get();

//     return $user;

//     // foreach ($user->roles as $role) {
//     //     echo $role->name;
//     // }
// });

// Accessing the intermediate table / pivot

// Route::get('user/pivot', function () {
//     $user = User::find(1);

//     foreach ($user->roles as $role) {
//         echo $role->pivot->created_at;
//     }
// });

// Has Many Through
// Route::get('user/country', function () {
//     $country = Country::find(3);
//     // return $country;
//     foreach ($country->posts as $post) {
//         return $post->title;
//     }
// });

// Polymorphic Relations
// Route::get('user/photos', function () {
//     $user = User::find(1);

//     foreach ($user->photo as $photo) {
//         echo $photo->path . "<br>";
//     }
// });

// Route::get('post/photos', function () {
//     $post = Post::find(1);

//     foreach ($post->photo as $photo) {
//         echo $photo->path . "<br>";
//     }
// });

// Route::get('photo/{id}/post', function ($id) {
//     $photo = Photo::findOrFail($id);

//     return $photo->imageable;

// });

// Polymorphic Many to Many
// Route::get('/tag/video/{id}', function ($id) {
//     $tag = Tag::find($id);

//     foreach ($tag->videos as $video) {
//         return $video;
//     }
// });

// Route::get('tag/post', function () {
//     $tag = Tag::find(2);

//     foreach ($tag->posts as $post) {
//         return $post->title;
//     }
// });



