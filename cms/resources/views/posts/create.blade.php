@extends('layouts.app')

@section('title', 'Create Post')

@section('content')
    <h1>Create Post</h1>
    <form action="/posts" method="POST" enctype="multipart/form-data">
        @csrf

        

        <input type="text" name="title" id="title" placeholder="Enter Title"><br>
        @error('title')
            <div class="danger">{{ $message }}</div>
        @enderror()

        <textarea name="content" id="content" cols="30" rows="10"></textarea><br>
        @error('content')
            <div class="danger">{{ $message }}</div>
        @enderror()

        <input type="file" name="image_path" id="image_path"><br>
        @error('image_path')
            <div class="danger">{{ $message }}</div>
        @enderror()

        <input type="submit" value="Submit">
    </form>

@endsection