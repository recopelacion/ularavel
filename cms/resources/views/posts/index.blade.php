@extends('layouts.app')

@section('title', 'All Post')

@section('content')
    <h1>All Posts</h1>
    <a href="{{ route('posts.create') }}">Add Post</a>
    @if (session('status'))
        <div class="alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (count($posts) > 0)
        @foreach ($posts as $post)
            <a href="{{ route('posts.show', $post->id) }}"> <b><h4>{{ $post->title }}</h4></b> </a>
        @endforeach
    @else
        <p>No posts found</p>
    @endif
    
@endsection