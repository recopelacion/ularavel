


@extends('layouts.app')

@section('title', 'The Post')

@section('content')
    <div class="img">
    <img src="/storage/post_images/{{ $post->image_path }}" alt="">
    </div>
    <h4>{{ $post->title }}</h4>
    <p>{{ $post->content }}</p>
    <small>Written on: {{ $post->created_at->format('M d Y') }} | Written by: {{ $post->user->name }}</small><br>
    <a href="{{ route('posts.edit', $post->id) }}">Edit Post</a>

    <form action="/posts/{{ $post->id }}" method="post">
        @csrf
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" value="Delete">
    </form>
@endsection