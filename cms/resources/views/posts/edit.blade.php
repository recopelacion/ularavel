@extends('layouts.app')

@section('title', 'Edit Post')

@section('content')
    <h1>Edit Post</h1>
    <form action="/posts/{{ $post->id }}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <input type="text" name="title" id="title" placeholder="Enter Title" value="{{ $post->title }}">
        <textarea name="content" id="content" cols="30" rows="10">{{ $post->content }}</textarea>
        <input type="file" name="image_path" id="image_path" value="{{ $post->image_path }}"><br>
        <input type="submit" value="Submit">
    </form>
@endsection