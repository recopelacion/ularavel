<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    // Contact Controller
    public function contact(){

        // $peoples = ['Robert', 'John', 'Fil', 'Frank', 'Yna', 'Monica'];
        // $peoples = [];

        // return view('pages.contact')->with('peoples', $peoples);
        // return view('pages.contact', compact('peoples'));

        return view('pages.contact');
    }
}
