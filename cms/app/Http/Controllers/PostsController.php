<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;
use App\Http\Requests;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $posts = Post::all()->orderBy('title', 'desc');
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {

        // $this->validate($request, [
        //     'title' => 'required|unique:posts|min:5|max:255',
        //     'content' => 'required|min:5'
        // ]);

        // Handle the file upload
        if( $request->hasFile('image_path') ){
            // get the file name with extension
            $fileNameExtension = $request->file('image_path')->getClientOriginalName();

            // get just the file name
            $fileName = pathinfo($fileNameExtension, PATHINFO_FILENAME);

            // get the extension
            $extension = $request->file('image_path')->getClientOriginalExtension();

            // file name to store
            $fileNameStore= $fileName.'_'.time().'.'.$extension;

            // upload the image
            $path = $request->file('image_path')->storeAs('public/post_images', $fileNameStore);
        }else{
            $fileNameStore = 'noimage.jpg';
        }

        $post = new Post;
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->image_path = $fileNameStore;
        $post->user_id = 1;
        $post->save();

        return redirect('/posts')->with('status', 'Post created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // Handle the file upload
        if( $request->hasFile('image_path') ){
            // get the file name with extension
            $fileNameExtension = $request->file('image_path')->getClientOriginalName();

            // get just the file name
            $fileName = pathinfo($fileNameExtension, PATHINFO_FILENAME);

            // get the extension
            $extension = $request->file('image_path')->getClientOriginalExtension();

            // file name to store
            $fileNameStore= $fileName.'_'.time().'.'.$extension;

            // upload the image
            $path = $request->file('image_path')->storeAs('public/post_images', $fileNameStore);
        }

        $post = Post::find($id);

        $post->title = $request->input('title');
        $post->content = $request->input('content');

        if( $request->hasFile('image_path') ){
            $post->image_path = $fileNameStore;
        }

        $post->save();

        return redirect('/posts')->with('status', 'Post updated successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if( $post->image_path != 'noimage.jpg' ){
            Storage::delete('public/post_images/' . $post->image_path);
        }

        $post->forceDelete();

        return redirect('/posts')->with('status', 'Post deleted successfully!');
    }
}
